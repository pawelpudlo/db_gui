package file;
import javax.swing.*;
import java.awt.*;

public class Menu {

    private JFrame frame = new JFrame();
    private ImageProcessing imageProcessing = new ImageProcessing();


    public Menu() {
        imageProcessing.setVisible(true);
        frame.add(imageProcessing);

        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setTitle("Cocktail DataBase manager");


        frame.pack();
        frame.repaint();



    }


    public void run() {
        imageProcessing.repaint();

    }


}
