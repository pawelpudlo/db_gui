package file;

import dto.UserDto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ConnectorImpl implements Connector {

    private String passDB = "lol123123";
    private String loginDB = "postgres";
    private String urlDB = "jdbc:postgresql://localhost:5432/postgres";
    private String driver = "org.postgresql.Driver";
    private Connection con;
    private PreparedStatement stmt;
    private UserDto userDto;
    private String info;
    ResultSet rs;

    ConnectorImpl(String login, String pass) {
        this.info = "";
        this.passDB = pass;
        this.loginDB = login;
        connect();
    }

    private void connect() {
        try {
            Class.forName(this.driver);
            this.con = DriverManager.getConnection(this.urlDB, this.loginDB, this.passDB);
            this.info = "You connect";

        } catch (Exception var4) {
            System.out.println(var4.getMessage());
            this.info = "Bad login or password";
        }
    }

    public List<UserDto> getUser(){
        List users = new ArrayList<UserDto>();

        try{

            stmt = con.prepareStatement("SELECT * FROM users ORDER BY user_id");

            rs = stmt.executeQuery();

            while (rs.next()){
                users.add(new UserDto(rs.getInt(1),rs.getInt(2),rs.getString(3)));
            }
            refresh();
            return users;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            refresh();
            return users;
        }


    }

    @Override
    public void setRange(int range,String nick) {
        String query = String.format("UPDATE users SET range=%2d WHERE user_name='%s' ",range,nick);

        try {
            stmt = this.con.prepareStatement(query);
            stmt.execute();
        }catch (Exception var4){
            System.out.println(var4.getMessage());

        }
        refresh();
    }

    @Override
    public void setNick(String oldNick, String newNick) {

    }

    @Override
    public String deleteUser(int id,int range) {
        if(range == 2){
            return "You don't can delete this user";
        }
        try{
        String query = String.format(" DELETE FROM users WHERE user_id= %2d ",id);
            stmt = this.con.prepareStatement(query);
            stmt.execute();

            return "You was delete user";
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return "DataBase Error";
        }

    }

    public void refresh(){
        try{
            con.close();
            this.con = DriverManager.getConnection(this.urlDB, this.loginDB, this.passDB);
        }catch (Exception var4){
            System.out.println(var4.getMessage());
        }
    }

    @Override
    public void disconnect() {
        try {
            this.con.close();
            this.info = "You are disconnet";
        }catch (Exception var4){
            System.out.println(var4.getMessage());
        }

    }

    public String getInfo() {
        return info;
    }
}
