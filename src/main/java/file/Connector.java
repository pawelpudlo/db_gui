package file;

import dto.UserDto;

import java.util.List;

public interface Connector {

    List<UserDto> getUser();

    void setRange(int range,String nick);

    void setNick(String oldNick, String newNick);

    String deleteUser(int id,int range);

    void refresh();

    void disconnect();

}
