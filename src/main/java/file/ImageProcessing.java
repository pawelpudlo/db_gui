
package file;

import dto.UserDto;
import javafx.scene.layout.Border;

import javax.swing.*;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;


public class ImageProcessing extends JPanel implements ActionListener, MouseListener {

    private ConnectorImpl con;

    private final int winSizeX = 300, winSizeY = 320;

    private JTextField login;
    private JPasswordField password;

    private JLabel text = new JLabel();
    private JLabel nickText = new JLabel();
    private JLabel rangeText = new JLabel();
    private JLabel delteText = new JLabel();
    private JLabel dataBaseLogin = new JLabel();
    private JLabel dataBasePassword = new JLabel();

    private JButton buttonSet;
    private JButton buttonSet2;
    private JButton buttonDelete;
    private JButton buttonloginDB;
    private JButton buttonDisconnectDB;

    // Belt for set User
    private JComboBox<String> comboLanguageNick;
    private JComboBox<String> comboLanguageRange;

    private List<UserDto> users;

    private String id = "";
    private String nick = "";
    private String range = "";
    private String info = "";
    private List<String> allRang;

    private Character[] alfabet;
    private Character[] number;

    public ImageProcessing() {

        this.alfabet = new Character[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        this.number = new Character[]{'0','1','2','3','4','5','6','7','8','9',};

        this.dataBaseLogin.setText("Login Data Base:");
        this.dataBasePassword.setText("Password Data Base:");
        this.dataBaseLogin.setBounds(1,110,130,20); //120
        this.dataBasePassword.setBounds(1, 160,130,20);
        add(dataBaseLogin);
        add(dataBasePassword);

        this.login = new JTextField();
        this.login.setBounds(1,135,130,20);
        add(login);

        this.password = new JPasswordField();
        this.password.setBounds(1,185,130,20);
        add(password);


        //this.login.setBounds();


        this.allRang =new ArrayList<>();
        this.allRang.add("Normal user");
        this.allRang.add("Drink Expert");
        this.allRang.add("Admin");





        this.comboLanguageNick = new JComboBox<String>();
        this.comboLanguageRange = new JComboBox<>();




        this.comboLanguageRange.addItem(this.allRang.get(0));
        this.comboLanguageRange.addItem(this.allRang.get(1));
        this.comboLanguageRange.addItem(this.allRang.get(2));

        this.comboLanguageNick.setBounds(81,20,150,20);
        this.comboLanguageRange.setBounds(81,50,150,20);
        add(comboLanguageNick);
        add(comboLanguageRange);


        //View panel for data
        this.addMouseListener(this);
        this.setLayout(null);

        text.setBounds(0, 220, 300, 100);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        text.setVerticalAlignment(SwingConstants.CENTER);
        this.add(text);

        nickText.setBounds(1,20,80,20);
        nickText.setText("User name:");
        add(nickText);

        rangeText.setBounds(1,50,80,20);
        rangeText.setText("User range:");
        add(rangeText);

        delteText.setText("Delte User:");
        delteText.setBounds(1,80,80,20);
        add(delteText);

        comboLanguageRange.setEnabled(false);
        comboLanguageNick.setEnabled(false);




    }





    public void actionPerformed(ActionEvent actionEvent) {

    }


    public void mouseClicked(MouseEvent mouseEvent) {

    }


    public void mousePressed(MouseEvent mouseEvent) {

    }

    public void mouseReleased(MouseEvent mouseEvent) {

    }


    public void mouseEntered(MouseEvent mouseEvent) {

    }


    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(winSizeX, winSizeY);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.setStroke(new BasicStroke(2));

        drawInfobox(g2);
        addButtons();
    }





    private void drawInfobox(Graphics2D g2) {
        g2.setColor(new Color(0, 0, 0));
        g2.drawRect(0, 219, 300, 100);


        if (true) {

            String info = "<html> User " +
                    "<br> Nick: " + this.nick+
                    "<br> Range: " + this.range +
                    "<br> Id: " + this.id +
                    "<br> info: " + this.info +
                    "</html>";

            text.setText(info);

        } else {
            text.setText("Select field to show more info");
        }
    }



    private void addButtons() {
        int x = 235;
        int y = 20;
        int width = 60;
        int height = 20;

        //Button for set user
        this.buttonSet = new JButton("SET");
        this.buttonSet2 = new JButton("SET");
        this.buttonDelete = new JButton("DELETE USER");
        this.buttonDisconnectDB = new JButton("DISCONNECT");
        this.buttonloginDB = new JButton("CONNECT");


        this.buttonSet.setBounds(x, y, width, height);
        this.buttonSet2.setBounds(x, y * 2 + 10, width, height);
        this.buttonDelete.setBounds(81, y * 4, 215, height);
        this.buttonloginDB.setBounds(132, 136, 160, 19);
        this.buttonDisconnectDB.setBounds(132, 186, 160, 19);


        this.buttonDelete.setEnabled(false);

        add(buttonSet);
        add(buttonSet2);
        add(buttonDelete);
        add(buttonloginDB);
        add(buttonDisconnectDB);


        this.buttonSet.addActionListener(e -> {

            this.nick = String.valueOf(comboLanguageNick.getSelectedItem());

            if (!this.nick.equals("")) {

                for (UserDto user : this.users) {

                    if (this.nick.equals(user.getNick())) {
                        this.id = String.valueOf(user.getId());

                        if (user.getRange() == 0) {
                            this.range = this.allRang.get(0);
                        } else if (user.getRange() == 1) {
                            this.range = this.allRang.get(1);
                        } else if (user.getRange() == 2) {
                            this.range = this.allRang.get(2);
                        }
                    }
                }
                this.info = "You was check user";
                comboLanguageRange.setEnabled(true);
            }
        });

        this.buttonSet2.addActionListener(e -> {

            if (!this.nick.equals("")) {
                this.range = String.valueOf(comboLanguageRange.getSelectedItem());
                int range = 0;

                if (this.range.equals(this.allRang.get(1))) {
                    range = 1;
                } else if (this.range.equals(this.allRang.get(2))) {
                    range = 2;
                }

                this.con.setRange(range, this.nick);
                this.info = "You update rang";
            } else {
                this.info = "Please check user";
            }
        });

        this.buttonDelete.addActionListener(e -> {
            if (!this.nick.equals("")) {
                int range = 0;

                if (this.range.equals(this.allRang.get(1))) {
                    range = 1;
                } else if (this.range.equals(this.allRang.get(2))) {
                    range = 2;
                }

                this.info = con.deleteUser(Integer.valueOf(this.id), range);
                if (this.info.equals("You don't can delete this user")) {
                    this.comboLanguageRange.setEnabled(true);
                } else {
                    this.comboLanguageRange.setEnabled(false);
                }
                this.nick = "";
                this.range = "";
                this.users.remove(Integer.valueOf(this.id));
                this.comboLanguageNick.removeItem((Integer.valueOf(this.id)));
                this.id = "";


            } else {
                this.info = "Please check user";
            }
        });

        this.buttonloginDB.addActionListener(e -> {
            String log;
            String password;
            String login;
            String pass;
            int value = 0;
            int value1 = 0;
            log = this.login.getText();
            password = this.password.getText();
            login = log.toUpperCase();
            pass = password.toUpperCase();
            if (log.equals("") || password.equals("")) {
                this.info = "Please make up login or password";
            } else {

                char sign;
                char signPass;
                for (int i = 0; i < login.length(); i++) {
                    sign = login.charAt(i);
                    for (int j = 0; j < this.alfabet.length; j++) {
                        if(sign == this.alfabet[j]){
                            value++;
                        }
                    }
                    for (int jj = 0; jj < this.number.length; jj++) {
                        if(sign == this.number[jj]){
                            value++;
                        }
                    }
                }

                for (int z = 0; z < pass.length(); z++) {
                    signPass = pass.charAt(z);
                    for (int j = 0; j < this.alfabet.length; j++) {
                        if(signPass == this.alfabet[j]){
                            value1++;
                        }
                    }
                    for (int jj = 0; jj < this.number.length; jj++) {
                        if(signPass == this.number[jj]){
                            value1++;
                        }
                    }
                }

                if(value != log.length() || value1 != password.length()){
                    this.info = value1 + " " + value + " " + log + " " + password;
                }else{
                    this.con = new ConnectorImpl(log,password);
                    this.info = this.con.getInfo();
                    this.users = new ArrayList<>(con.getUser());
                    for (UserDto user : users) {
                        this.comboLanguageNick.addItem(user.getNick());
                    }
                    this.comboLanguageNick.setEnabled(true);
                    this.buttonloginDB.setEnabled(false);
                }

            }

            this.buttonDelete.setEnabled(true);
        });

        this.buttonDisconnectDB.addActionListener(e ->{
            this.con.disconnect();
            this.info = this.con.getInfo();
            this.comboLanguageNick.setEnabled(false);
            this.comboLanguageRange.setEnabled(false);
            this.login.setText("");
            this.password.setText("");
            this.buttonDelete.setEnabled(false);

        });
    }

}
