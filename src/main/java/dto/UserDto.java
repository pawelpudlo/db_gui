package dto;

public class UserDto {

    private int id;
    private int range;
    private String nick;

    public UserDto(int id, int range, String nick) {
        this.id = id;
        this.range = range;
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
}
